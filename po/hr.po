# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Milo Ivir <mail@milotype.de>, 2020, 2021, 2022, 2023, 2024.
# Danial Behzadi <dani.behzi@ubuntu.com>, 2020, 2021, 2022, 2023, 2024.
# Weblate Translation Memory <noreply-mt-weblate-translation-memory@weblate.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: dani.behzi@ubuntu.com\n"
"POT-Creation-Date: 2024-10-28 21:19+0330\n"
"PO-Revision-Date: 2024-10-30 12:14+0000\n"
"Last-Translator: Weblate Translation Memory <noreply-mt-weblate-translation-"
"memory@weblate.org>\n"
"Language-Team: Croatian <https://hosted.weblate.org/projects/carburetor/"
"translations/hr/>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Weblate 5.8.2-dev\n"

#: data/applications/io.frama.tractor.carburetor.desktop.in:5
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:5
#: data/gtk/main.ui:46
msgid "Carburetor"
msgstr "Carburetor"

#: data/applications/io.frama.tractor.carburetor.desktop.in:6
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:6
msgid "Browse anonymously"
msgstr ""

#: data/applications/io.frama.tractor.carburetor.desktop.in:12
msgid "Tor;Tractor;Carburetor;"
msgstr "Tor;Tractor;Carburetor;"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:9
msgid ""
"Discover anonymous browsing with Carburetor on your phones and computers. "
"Tailored for GNOME, it's your hidden superhero for online adventures, "
"powered by TOR. Carburetor provides a local TOR proxy that hides your IP, "
"ensuring your Internet activities remain encrypted, anonymized, and "
"untraceable. Don't get your hands dirty with system files anymore – just tap "
"into the app, keeping your online world safe and private. Customize settings "
"if you want, but you don't have to. Carburetor is Free Software and puts you "
"in control. No worries, just enjoy your anonymous browsing!"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:17
msgid "Tor"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:18
msgid "Tractor"
msgstr "Tractor"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:41
#, fuzzy
msgid "Tractor Team"
msgstr "Za Tractor"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:45
msgid "Main Page of Carburetor in Desktop mode"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:50
msgid "Carburetor preferences in dark mode"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:70
msgid "Updated to the GNOME 47 platform."
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:71
msgid "Updated translations."
msgstr ""

#: data/gtk/help-overlay.ui:28
msgctxt "ShortcutsGroup title"
msgid "General"
msgstr "Opće"

#: data/gtk/help-overlay.ui:32
msgctxt "shortcut"
msgid "Preferences"
msgstr "Postavke"

#: data/gtk/help-overlay.ui:38
msgctxt "shortcut"
msgid "Show Shortcuts"
msgstr "Pokaži prečace"

#: data/gtk/help-overlay.ui:44
msgctxt "shortcut"
msgid "Quit"
msgstr "Zatvori"

#: data/gtk/help-overlay.ui:51
msgctxt "ShortcutsGroup title"
msgid "Functionality"
msgstr "Funkcionalnost"

#: data/gtk/help-overlay.ui:55
msgctxt "shortcut"
msgid "Connect / Disconnect"
msgstr "Spoji/Odspoji"

#: data/gtk/help-overlay.ui:61
msgctxt "shortcut"
msgid "New ID"
msgstr "Novi ID"

#: data/gtk/help-overlay.ui:68
msgctxt "ShortcutsGroup title"
msgid "Logs"
msgstr "Zapisi"

#: data/gtk/help-overlay.ui:72
msgctxt "shortcut"
msgid "Clear Logs"
msgstr "Izbriši zapise"

#: data/gtk/help-overlay.ui:78
#, fuzzy
msgctxt "shortcut"
msgid "Copy Logs"
msgstr "Izbriši zapise"

#: data/gtk/logs.ui:31 data/gtk/main.ui:55
msgid "Logs"
msgstr "Zapisi"

#: data/gtk/logs.ui:38
msgctxt "tooltip"
msgid "Clear"
msgstr "Izbriši"

#: data/gtk/logs.ui:45
msgctxt "tooltip"
msgid "Copy"
msgstr "Kopiraj"

#: data/gtk/main.ui:52
msgctxt "tooltip"
msgid "More Options"
msgstr "Više opcija"

#: data/gtk/main.ui:63
msgctxt "tooltip"
msgid "Main Menu"
msgstr "Glavni izbornik"

#: data/gtk/main.ui:78 src/ui.py:120
msgid "_Connect"
msgstr "_Poveži"

#: data/gtk/main.ui:82 src/ui.py:118
msgid "Press the \"Connect\" button to start"
msgstr "Za pokretanje pritisni gumb „Poveži”"

#: data/gtk/main.ui:84 src/ui.py:117
msgid "Stopped"
msgstr "Prekinuto"

#: data/gtk/main.ui:102
msgctxt "Action Menu Item"
msgid "_New ID"
msgstr "_Novi ID"

#: data/gtk/main.ui:106
msgctxt "Action Menu Item"
msgid "_Check connection"
msgstr "_Provjeri vezu"

#: data/gtk/main.ui:110
msgctxt "Action Menu Item"
msgid "_Toggle proxy on system"
msgstr "_Uklj./Isklj. proxy na sustavu"

#: data/gtk/main.ui:116
msgctxt "App Menu Item"
msgid "_Preferences"
msgstr "_Postavke"

#: data/gtk/main.ui:120
msgctxt "App Menu Item"
msgid "_Keyboard Shortcuts"
msgstr "_Tipkovni prečaci"

#: data/gtk/main.ui:124
msgctxt "App Menu Item"
msgid "_About Carburetor"
msgstr "_Carburetor podaci"

#: data/gtk/main.ui:128
msgctxt "App Menu Item"
msgid "_Quit"
msgstr "_Zatvori"

#: data/gtk/preferences.ui:26
msgctxt "PreferencesPage title"
msgid "General"
msgstr "Opće"

#: data/gtk/preferences.ui:29
msgctxt "PreferencesGroup title"
msgid "Exit Node"
msgstr "Čvor izlaza"

#: data/gtk/preferences.ui:33
msgid "The country you want to connect from"
msgstr "Zemlja iz koje se želiš povezati"

#: data/gtk/preferences.ui:34
msgid "Exit Country"
msgstr "Zemlja izlaza"

#: data/gtk/preferences.ui:44
msgid "Country Code"
msgstr "Kod zemlje"

#: data/gtk/preferences.ui:52
msgctxt "PreferencesGroup title"
msgid "Connections"
msgstr "Veze"

#: data/gtk/preferences.ui:55
msgid "Allow external devices to use this network"
msgstr "Dopusti vanjskim uređajima koristiti ovu mrežu"

#: data/gtk/preferences.ui:56
#, fuzzy
msgid "Accept Incoming Connections"
msgstr "Prihvati veze"

#: data/gtk/preferences.ui:62
msgid "Restrict connections to ports 80 and 443"
msgstr ""

#: data/gtk/preferences.ui:63
msgid "Enable Fascist Firewall Mode"
msgstr ""

#: data/gtk/preferences.ui:74
msgctxt "PreferencesPage title"
msgid "Proxy"
msgstr "Proxy"

#: data/gtk/preferences.ui:77
msgid "Ports on which the application listens for incoming connections."
msgstr ""

#: data/gtk/preferences.ui:78
#, fuzzy
msgid "Listening Ports"
msgstr "Ulaz osluškivanja"

#: data/gtk/preferences.ui:82
msgctxt "Preferences SpinRow subtitle"
msgid "Main connection point for secure communication"
msgstr "Glavna točka povezivanja za sigurnu komunikaciju"

#: data/gtk/preferences.ui:83
msgctxt "Preferences SpinRow title"
msgid "SOCKS"
msgstr "SOCKS"

#: data/gtk/preferences.ui:90
msgctxt "Preferences SpinRow subtitle"
msgid "A local DNS server for enhanced privacy"
msgstr "Lokalni DNS server za povećanu privatnost"

#: data/gtk/preferences.ui:91
msgctxt "Preferences SpinRow title"
msgid "DNS"
msgstr "DNS"

#: data/gtk/preferences.ui:98
msgctxt "Preferences SpinRow subtitle"
msgid "A fallback HTTP Tunnel for simple connections"
msgstr "Rezervni HTTP tunel za jednostavne veze"

#: data/gtk/preferences.ui:99
msgctxt "Preferences SpinRow title"
msgid "HTTP"
msgstr "HTTP"

#: data/gtk/preferences.ui:107
msgid "Options"
msgstr "Opcije"

#: data/gtk/preferences.ui:110
msgid "Automatically set proxy after successful connection"
msgstr ""

#: data/gtk/preferences.ui:111
#, fuzzy
msgid "Auto Set Proxy"
msgstr "Poništi proxy"

#: data/gtk/preferences.ui:122
msgctxt "PreferencesPage title"
msgid "Bridges"
msgstr "Mostovi"

#: data/gtk/preferences.ui:125
msgctxt "PreferencesGroup title"
msgid "Pluggable Transports"
msgstr "Priključivi Prijenosi"

#: data/gtk/preferences.ui:129
msgid "Type of Transport"
msgstr "Vrsta Prijenosa"

#: data/gtk/preferences.ui:136
msgid "Transport Executable File"
msgstr "Izvršna Datoteka Prijenosa"

#: data/gtk/preferences.ui:141 src/handler.py:247 src/ui.py:220
msgid "None"
msgstr "Ništa"

#: data/gtk/preferences.ui:153
msgid ""
"Bridges help you securely access the Tor Network in places where Tor is "
"blocked."
msgstr ""

#: data/gtk/preferences.ui:154
msgctxt "PreferencesGroup title"
msgid "Bridges"
msgstr "Mostovi"

#: data/gtk/preferences.ui:175 src/handler.py:359
msgid "_Add"
msgstr "_Dodaj"

#: data/gtk/preferences.ui:187
msgid "View as a text file to edit and share bridges"
msgstr ""

#: data/gtk/preferences.ui:188
msgid "_Open Externally"
msgstr ""

#: data/gtk/preferences.ui:201
msgid ""
"Since many bridge addresses aren’t public, you may need to request some from "
"the Tor Project."
msgstr ""

#: data/gtk/preferences.ui:202
msgctxt "PreferencesGroup title"
msgid "Find More Bridges"
msgstr ""

#: data/gtk/preferences.ui:207
msgid "Telegram"
msgstr "Telegram"

#: data/gtk/preferences.ui:219
msgid "Web"
msgstr ""

#: data/gtk/preferences.ui:231
msgid "Gmail or Riseup"
msgstr ""

#: src/actions.py:91
msgid "translator-credits"
msgstr "Milo Ivir <mail@milotype.de>"

#: src/actions.py:113
msgid "_Cancel"
msgstr "_Odustani"

#: src/actions.py:123
msgid "Disconnecting…"
msgstr "Odspajanje …"

#: src/actions.py:127
msgid "Connecting…"
msgstr "Povezivanje …"

#: src/actions.py:171
msgid "You have a new identity!"
msgstr "Imaš novi identitet!"

#: src/actions.py:173
msgid "Tractor is not running!"
msgstr "Tractor nije pokrenut!"

#: src/actions.py:199
msgid "Proxy has been unset"
msgstr "Proxy je deaktiviran"

#: src/actions.py:202
msgid "Proxy has been set"
msgstr "Proxy je aktiviran"

#: src/actions.py:279
msgid "No relevant bridges found"
msgstr "Nije pronađen nijedan relevantni most"

#: src/actions.py:294
msgid "Transport executable not found"
msgstr "Izvršna datoteka prijenosa nije pronađena"

#: src/actions.py:365
msgid "Your connection is secure"
msgstr "Tvoja je veza sigurna"

#: src/actions.py:368
msgid "Failed to connect to TOR network"
msgstr "Neuspjelo povezivanje s TOR mrežom"

#: src/actions.py:374
msgid "Connection check complete"
msgstr "Provjera veze je završena"

#: src/actions.py:378
msgid "Try again"
msgstr "Pokušaj ponovo"

#: src/actions.py:379
msgid "Quit"
msgstr "Zatvori"

#: src/handler.py:134
msgid "Auto (Best)"
msgstr "Automatski (najbolje)"

#: src/handler.py:138
msgid "Other (Manual)"
msgstr "Drugo (ručno)"

#: src/handler.py:248 src/handler.py:309
msgid "Vanilla"
msgstr "Vanilla"

#: src/handler.py:249 src/handler.py:311
msgid "Obfuscated"
msgstr "Pomućeno"

#: src/handler.py:250 src/handler.py:313
msgid "Snowflake"
msgstr "Snowflake"

#: src/handler.py:251 src/handler.py:315
msgid "Conjure"
msgstr ""

#: src/handler.py:324
msgid "Connected"
msgstr "Povezan"

#: src/handler.py:340
msgid "Remove"
msgstr "Ukloni"

#: src/handler.py:408
msgid "Duplicate"
msgstr "Dupliciraj"

#: src/ui.py:142
msgid "Running"
msgstr "Pokrenuto"

#: src/ui.py:143
msgid "Socks Port"
msgstr "Priključak za Socks"

#: src/ui.py:144
msgid "DNS Port"
msgstr "Priključak za DNS"

#: src/ui.py:145
msgid "HTTP Port"
msgstr "Priključak za HTTP"

#: src/ui.py:156
msgid "_Disconnect"
msgstr "_Odspojeno"

#: src/ui.py:173
msgid "Carburetor is connected"
msgstr "Carburetor je povezan"

#: src/ui.py:175
#, fuzzy
msgid "Proxy has been set too."
msgstr "Proxy je aktiviran"

#: src/ui.py:178
msgid "You may want to use the ports or toggle the proxy now."
msgstr "Možda želiš koristiti priključke ili uključi/isključi proxy sada."

#: src/ui.py:180
msgid "Toggle Proxy"
msgstr "Uključi/Isključi proxy"

#~ msgctxt "PreferencesPage title"
#~ msgid "Ports"
#~ msgstr "Priključci"

#~ msgid "WebTunnel"
#~ msgstr "WebTunnel"

#, fuzzy
#~ msgid "_Add Bridge"
#~ msgstr "Mostovi"

#~ msgid "Please copy and paste the bridges you have in the area below."
#~ msgstr "Kopiraj i umetni mostove koje imaš u donje područje."

#~ msgid "_Save"
#~ msgstr "_Spremi"

#~ msgid "Save Bridges as a file in your local configs"
#~ msgstr "Spremi mostove kao datoteku u lokalnim konfiguracijama"

#~ msgid "_OK"
#~ msgstr "_U redu"

#~ msgid ""
#~ "To Get bridges, you have these options:\n"
#~ "\n"
#~ "• Visit the Tor website: <a href=\"https://bridges.torproject.org/options/"
#~ "\">https://bridges.torproject.org/options</a>\n"
#~ "• Use the Tor bridges Telegram bot: <a href=\"https://t.me/"
#~ "GetBridgesBot\">Tor bridges Telegram bot</a>\n"
#~ "• Send an email from a Riseup or Gmail address to: <a href=\"mailto:"
#~ "bridges@torproject.org?body=get bridges\">bridges@torproject.org</a>"
#~ msgstr ""
#~ "Za dobivanje mostova postoje sljedeće opcije:\n"
#~ "\n"
#~ "• Posjeti web stranicu Tor: <a href=\"https://bridges.torproject.org/"
#~ "options/\">https://bridges.torproject.org/options</a>\n"
#~ "• Koristi Tor mostove Telegram bot: <a href=\"https://t.me/"
#~ "GetBridgesBot\">Tor mostovi Telegram bot</a>\n"
#~ "• Pošalji e-mail s jedne Riseup ili Gmail adrese na: <a href=\"mailto:"
#~ "bridges@torproject.org?body=get bridges\">bridges@torproject.org</a>"

#~ msgid ""
#~ "Please check the bridges to match the selected pluggable transport type."
#~ msgstr ""
#~ "Provjeri mostove da odgovaraju odabranoj priključivoj vrsti prijenosa."

#~ msgid "_Logs"
#~ msgstr "_Zapisi"

#~ msgid "Austria"
#~ msgstr "Austrija"

#~ msgid "Bulgaria"
#~ msgstr "Bugarska"

#~ msgid "Canada"
#~ msgstr "Kanada"

#~ msgid "Switzerland"
#~ msgstr "Švicarska"

#~ msgid "Czech"
#~ msgstr "Češka"

#~ msgid "Germany"
#~ msgstr "Njemačka"

#~ msgid "Spain"
#~ msgstr "Španjolska"

#~ msgid "Finland"
#~ msgstr "Finska"

#~ msgid "France"
#~ msgstr "Francuska"

#~ msgid "Ireland"
#~ msgstr "Irska"

#~ msgid "Moldova"
#~ msgstr "Moldavija"

#~ msgid "Netherlands"
#~ msgstr "Nizozemska"

#~ msgid "Norway"
#~ msgstr "Norveška"

#~ msgid "Poland"
#~ msgstr "Poljska"

#~ msgid "Romania"
#~ msgstr "Rumunjska"

#~ msgid "Seychelles"
#~ msgstr "Sejšeli"

#~ msgid "Sweden"
#~ msgstr "Švedska"

#~ msgid "Singapore"
#~ msgstr "Singapur"

#~ msgid "Russia"
#~ msgstr "Rusija"

#~ msgid "Ukraine"
#~ msgstr "Ukrajina"

#~ msgid "United Kingdom"
#~ msgstr "Ujedinjeno Kraljevstvo"

#~ msgid "United States"
#~ msgstr "Sjedinjene Države"

#~ msgid "Tractor couldn't connect"
#~ msgstr "Tractor se nije uspio povezati"

#~ msgid "Tractor is stopped"
#~ msgstr "Tractor je prekinut"

#~ msgid "Tractor is running"
#~ msgstr "Tractor je pokrenut"

#~ msgctxt "tooltip"
#~ msgid "Show Details"
#~ msgstr "Pokaži detalje"

#~ msgid "Local port on which Tractor would be listen"
#~ msgstr "Lokalni priključak na kojem se Tractor prisluškuje"

#~ msgid "Local port on which you would have an anonymous name server"
#~ msgstr "Lokalni priključak na kojem se može nalaziti anonimni poslužitelj"

#~ msgid "Local port on which a HTTP tunnel would be listen"
#~ msgstr "Lokalni priključak na kojem se HTTP tunel prisluškuje"

#~ msgid "Can not save the bridges"
#~ msgstr "Nije moguće spremiti mostove"

#~ msgid "Please check the syntax of bridges"
#~ msgstr "Provjeri sintaksu mostova"

#~ msgid "Type"
#~ msgstr "Vrsta"

#~ msgid "Type of bridge"
#~ msgstr "Vrsta mosta"

#~ msgid "Client Transport Plugin"
#~ msgstr "Dodatak klijentskog transporta"

#~ msgid "GTK frontend for Tractor"
#~ msgstr "GTK sučelje za Tractor"

#~ msgid "Copyright © 2019-2023, Tractor Team"
#~ msgstr "Autorska prava © 2019. – 2023., Tractor tim"

#~ msgid "Use obfs4proxy"
#~ msgstr "Koristi obfs4proxy"

#~ msgid "Please specify the plugin executable file"
#~ msgstr "Odredi izvršnu datoteku dodatka"

#~ msgid "Start"
#~ msgstr "Pokreni"

#~ msgid "Stop"
#~ msgstr "Prekini"

#~ msgid "stopping…"
#~ msgstr "prekidanje …"

#~ msgid "starting…"
#~ msgstr "pokretanje …"

#~ msgid "<b>Use bridges</b>"
#~ msgstr "<b>Koristi mostove</b>"

#~ msgid "<small>Bridges help you to bypass tor sensorship</small>"
#~ msgstr "<small>Mostovi pomažu zaobići cenzuru</small>"

#~ msgid "Bridges:"
#~ msgstr "Mostovi:"

#~ msgid "<b>obfs4proxy executable</b>"
#~ msgstr "<b>Izvršna datoteka obfs4proxy</b>"

#~ msgid "<small>You should specify where is obfs4proxy file</small>"
#~ msgstr "<small>Odredi mjesto datoteke obfs4proxy</small>"
