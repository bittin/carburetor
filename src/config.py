# Part of Carburetor project
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020-2024.

"""
main configurations for carburetor
"""

import gettext
import os

import pycountry
from gi.repository import Gio


# Get the user's locale
language = os.environ.get("LANG", "en_US.UTF-8").split(".")[0]
countries_l10n = gettext.translation(
    "iso3166-1", pycountry.LOCALES_DIR, [language]
)

COMMAND = "tractor"
DCONF = Gio.Settings.new("org.tractor")
NODES = {}
for code in ("ca", "ch", "de", "es", "gb", "it", "fr", "nl", "se", "us"):
    country = pycountry.countries.get(alpha_2=code)
    NODES[code] = countries_l10n.gettext(country.name) + " " + country.flag
